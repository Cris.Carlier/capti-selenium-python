import unittest
import time

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.common.alert import Alert

class Operaciones(unittest.TestCase):
    global driver

    @classmethod
    def setUpClass(self):
        # Iniciamos una variable global para la clase.
        global driver

        # Iniciamos el driver de selenium y la sesion del navegador.
        driver = self.iniciar_driver(self)

        # Navegamos a la pagina google.com
        driver.get("https://automationexercise.com/")

    @classmethod
    def tearDownClass(self):
        # Hacemos una breve pausa antes de realizar alguna accion
        time.sleep(3)

        # Cerramos la session del navegador.
        driver.close()

    def iniciar_driver(self):
        # Opciones para ejecutar en modo headless.
        options = Options()
        # options.add_argument('--headless')
        # options.add_argument('--disable-gpu')

        # Iniciamos el driver de chrome indicado la ruta del ejecutable.
        driver = webdriver.Chrome(executable_path='./driver/chromedriver.exe', options=options)

        # Declaramos una espera implicita de 30 segundos.
        driver.implicitly_wait(15)

        # Maximizamos la ventana de la sesion iniciada.
        driver.maximize_window()
        return driver

    def click(self, elemento):
        # Hacemos una breve pausa antes de realizar alguna accion.
        time.sleep(1)

        elemento_web = driver.find_element(By.XPATH, elemento)

        # Hacemos click en el elemento.
        elemento_web.click()

    def escribir(self, elemento, texto):
        # Hacemos una breve pausa antes de realizar alguna accion.
        time.sleep(1)

        # Borramos el contenido del campo de texto.
        elemento_web = driver.find_element(By.XPATH, elemento)
        elemento_web.clear()

        # Escribimos el texto indicado en el campo de texto.
        elemento_web.send_keys(texto)

    def validar_texto(self, elemento, esperado):
        # Hacemos una breve pausa antes de realizar alguna accion.
        time.sleep(1)
        elemento_web = driver.find_element(By.XPATH, elemento)

        # Leemos el contenido del elemento.
        encontrado = elemento_web.text

        # Verificar que el texto_real es igual al texto_esperado
        assert encontrado == esperado, f"El texto esperado era '{esperado}', pero el texto real fue '{encontrado}'"

    def alerta_ok(self):
        alert = Alert(driver)
        alert.accept()

    if __name__ == '__main__':
        unittest.main()
