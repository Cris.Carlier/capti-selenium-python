from random import randint

from page_objects.auto_practice_page import AutoPracticePage

class FlujoCommerce():

    def venta_con_producto(self, producto):
        AutoPracticePage.proceso_login_usuario(self)

        AutoPracticePage.abrir_catalogo_productos(self)

        AutoPracticePage.proceso_agregar_producto_carro(self, producto)

        AutoPracticePage.proceso_carro_compras_checkout(self, producto)

        AutoPracticePage.proceso_medio_pago(self)

        AutoPracticePage.proceso_finalizacion_compra(self)