import time
import unittest

from operaciones_internas.operaciones import Selenium
from selenium.common.exceptions import TimeoutException
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.wait import WebDriverWait


class Pruebas(unittest.TestCase):

    # Rutina ejecutada antes del inicio de los casos de prueba.
    @classmethod
    def setUpClass(self):
        # Iniciamos una variable global para la clase.
        global Driver

        # Iniciamos el driver de selenium y la sesion del navegador.
        Driver = Selenium.Iniciar_driver(self)

        # Navegamos a la pagina google.com
        Driver.get("http://www.weboo.cl/testing/")

    # Identificar elemento por id.
    def test_ejercicio_01(self):
        Elemento = Driver.find_element_by_id('username')
        Elemento.send_keys('Prueba busqueda ID')

    # Identificar elemento por nombre.
    def test_ejercicio_02(self):
        Elemento = Driver.find_element_by_name('user_name')
        Elemento.send_keys('Prueba busqueda NAME')

    # Identificar elemento por xpath.
    def test_ejercicio_03(self):
        Elemento = Driver.find_element_by_xpath('//*[@id="username"]')
        Elemento.send_keys('Prueba busqueda XPATH')

    # Usos de assertTrue y assertFail.
    def test_ejercicio_04(self):
        test_resultado = True # probar con True y False para ver resultados.
        self.assertFalse(test_resultado, "Este resultado debe ser falso (False)")
        self.assertTrue(test_resultado, "Este resultado debe ser verdadero (True)")

    # Usos de esperas explicitas.
    def test_ejercicio_05(self):
        Tiempo = 15

        try:
            Resultado = WebDriverWait(Driver, Tiempo).until(expected_conditions.presence_of_element_located((By.ID, "username2")))
            Resultado.send_keys('Prueba busqueda ID')

        except TimeoutException:
            print("No se encontro el elemento dentro del rango de tiempo")

    # Usos de listas desplegables (selects).
    def test_ejercicio_06(self):
        select = Select(Driver.find_element_by_id('mySelect'))

        time.sleep(3)
        select.select_by_index(1)

        time.sleep(2)
        select.select_by_value('saab')

        time.sleep(2)
        select.select_by_visible_text('Audi')

    # Busqueda elementos anidados.
    def test_ejercicio_07(self):
        ElementoParent = Driver.find_element_by_id('login')

        ElementoChild = ElementoParent.find_element_by_id('username')

        ElementoChild.send_keys('ElementoChild encontrado')

    # Subida de archivos.
    def test_ejercicio_08(self):
        Elemento = Driver.find_element_by_id('A')

        Elemento.send_keys('C:\Test\dummy.txt')

    # Ejecutar javascript, ejemplo de click
    def test_ejercicio_09(self):
        Driver.execute_script('document.getElementById("r3b545").click();')

    # Manejo de ventanas
    def test_ejercicio_10(self):
        ventana_principal = Driver.current_window_handle
        print(ventana_principal)

        Driver.execute_script('document.getElementById("idunico2").click();')
        ventanas = Driver.window_handles
        print(ventanas)

        time.sleep(6)
        Driver.switch_to.window(ventanas[1])
        time.sleep(6)
        Driver.switch_to.window(ventanas[0])
        time.sleep(6)

    # Manejo de frames
    def test_ejercicio_11(self):
        Driver.switch_to.frame(Driver.find_element_by_id('frameA'))
        Elemento = Driver.find_element_by_id('fa2')
        Elemento.send_keys('Estoy en el frame A')

        Driver.switch_to_default_content()

        time.sleep(2)
        Driver.switch_to.frame(Driver.find_element_by_id('frameB'))
        Elemento = Driver.find_element_by_id('fa2')
        Elemento.send_keys('Estoy en el frame B')

    # Manejo de alertas
    def test_ejercicio_12(self):
        Elemento = Driver.find_element_by_id('alerta')
        Elemento.click()

        time.sleep(1)
        Alerta = Driver.switch_to.alert

        time.sleep(2)
        Alerta.accept()

    # Acciones en cadena
    def test_ejercicio_13(self):
        Elemento1 = Driver.find_element_by_id('r3b545')
        Elemento2 = Driver.find_element_by_id('v4b324')
        Elemento3 = Driver.find_element_by_id('a1b162')
        Elemento4 = Driver.find_element_by_id('z6e908')

        action = ActionChains(Driver)
        action.move_to_element(Elemento1)
        action.click()
        action.move_to_element(Elemento2)
        action.click()
        action.move_to_element(Elemento3)
        action.click()
        action.move_to_element(Elemento4)
        action.click()
        action.perform()

    # Rutina ejecutada despues de finalizar con los casos de prueba.
    @classmethod
    def tearDownClass(self):
        # Hacemos una breve pausa antes de realizar alguna accion
        time.sleep(12)

        # Cerramos la session del navegador.
        Driver.close()


if __name__ == '__main__':
    unittest.main()