from operaciones_internas.operaciones import Operaciones
from funciones_negocios.parametros_commerce import ParametrosCommerce

class AutoPracticePage:
    homePage_products = "//*[@id='header']/div/div/div/div[2]/div/ul/li[2]/a"
    homePage_login = "//*[@id='header']/div/div/div/div[2]/div/ul/li[4]/a"
    homePage_cart = "//*[@id='header']/div/div/div/div[2]/div/ul/li[3]/a"
    cartModal_continueShopping = "//*[@id='cartModal']/div/div/div[3]/button"
    shoppingCartPage_proceedToCheckout = "//*[@id='do_action']/div[1]/div/div/a"
    loginPage_email = "//*[@id='form']/div/div/div[1]/div/form/input[2]"
    loginPage_pass = "//*[@id='form']/div/div/div[1]/div/form/input[3]"
    loginPage_loginButton = "//*[@id='form']/div/div/div[1]/div/form/button"
    checkoutPage_placeOrder = "//*[@id='cart_items']/div/div[7]/a"
    paymentPage_cardName = "//*[@id='payment-form']/div[1]/div/input"
    paymentPage_cardNumber = "//*[@id='payment-form']/div[2]/div/input"
    paymentPage_cvc = "//*[@id='payment-form']/div[3]/div[1]/input"
    paymentPage_expiration = "//*[@id='payment-form']/div[3]/div[2]/input"
    paymentPage_year = "//*[@id='payment-form']/div[3]/div[3]/input"
    paymentPage_payConfirmOrder = "//*[@id='submit']"
    orderPlacedPage_message = "//*[@id='form']/div/div/div/p"
    orderPlacedPage_continue = "//*[@id='form']/div/div/div/div/a"

    def abrir_catalogo_productos(self):
        Operaciones.click(self, AutoPracticePage.homePage_products)

    def proceso_login_usuario(self):
        Operaciones.click(self, AutoPracticePage.homePage_login)
        Operaciones.escribir(self, AutoPracticePage.loginPage_email, ParametrosCommerce.loginEmail)
        Operaciones.escribir(self, AutoPracticePage.loginPage_pass, ParametrosCommerce.loginPass)
        Operaciones.click(self, AutoPracticePage.loginPage_loginButton)

    def proceso_agregar_producto_carro(self, producto):
        Operaciones.click(self, "(//p[contains(text(),'" + producto + "')])[1]//following-sibling::a")
        Operaciones.click(self, AutoPracticePage.cartModal_continueShopping)

    def proceso_carro_compras_checkout(self, producto):
        Operaciones.click(self, AutoPracticePage.homePage_cart)
        Operaciones.click(self, AutoPracticePage.shoppingCartPage_proceedToCheckout)
        Operaciones.click(self, AutoPracticePage.checkoutPage_placeOrder)

    def proceso_medio_pago(self):
        Operaciones.escribir(self, AutoPracticePage.paymentPage_cardName, ParametrosCommerce.paymentCardName)
        Operaciones.escribir(self, AutoPracticePage.paymentPage_cardNumber, ParametrosCommerce.paymentCardNumber)
        Operaciones.escribir(self, AutoPracticePage.paymentPage_cvc, ParametrosCommerce.paymentCvc)
        Operaciones.escribir(self, AutoPracticePage.paymentPage_expiration, ParametrosCommerce.paymentExpiration)
        Operaciones.escribir(self, AutoPracticePage.paymentPage_year, ParametrosCommerce.paymentYear)
        Operaciones.click(self, AutoPracticePage.paymentPage_payConfirmOrder)

    def proceso_finalizacion_compra(self):
        Operaciones.click(self, AutoPracticePage.orderPlacedPage_continue)