from funciones_negocios.flujo_commerce import FlujoCommerce
from operaciones_internas.operaciones import Operaciones

class Pruebas(Operaciones):

	# Caso de prueba con dependencias ordenadas.
	def test_prueba_01(self):
		# Realizamos una venta en el flujo de ecommerce.
		FlujoCommerce.venta_con_producto(self, "Blue Top")

	# Caso de prueba con dependencias ordenadas.
	def test_prueba_02(self):
		# Realizamos una venta en el flujo de ecommerce.
		FlujoCommerce.venta_con_producto(self, "Men Tshirt")

	# Caso de prueba con dependencias ordenadas.
	def test_prueba_03(self):
		# Realizamos una venta en el flujo de ecommerce.
		FlujoCommerce.formulario_contacto(self)